#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include<string>
#include<algorithm>
#include<math.h>
#include <iomanip>
#include <stdio.h>
#include <numeric>
#include <Eigen/Core>
#include <Eigen/Dense>

using namespace std;

using namespace Eigen;

using Eigen::MatrixXd;

struct Data {
  vector<string> name;
  vector<double> avg_return;
  vector<double> std_deviation;  
};

struct frontier {
  vector<double> ror;
  vector<double> volatility;  
};