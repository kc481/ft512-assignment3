#include "efficient_frontier.hpp"

bool isnum(string s) {
  stringstream sin(s);
  double t;
  char p;
  if(!(sin >> t)) {
    return false;
  }
  if(sin >> p) {
    return false;
  }
  else {
    return true;
  }
}

MatrixXd xlumbda_calculation (Data data1, vector<vector<double>> correlation1, double return_rate) {
  int n = (int)data1.name.size();
  MatrixXd covariance_matrix(n,n);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      covariance_matrix(i, j) = correlation1[i][j] * data1.std_deviation[i] * data1.std_deviation[j];
    }
  }
  MatrixXd A(2,n);
  for (int i = 0; i < n; i++) {
    A(0, i) = 1;
    A(1, i) = data1.avg_return[i]; 
  } 
  MatrixXd b(1,n+2);
  for (int i = 0; i < (n + 2); i++) {
    b(0, i) = 0;
  }
  b(0, n) = 1;
  b = b.transpose();   
  MatrixXd big(n+2,n+2);
  Matrix2d zero;
  zero << 0, 0, 0, 0; 
  MatrixXd xlumbda(1,n+2);
  xlumbda = xlumbda.transpose();
  big << covariance_matrix, A.transpose(), A, zero;
  b((n + 1), 0) = return_rate;
  xlumbda = big.inverse() * b; 
  return xlumbda;
}

frontier restricted_calculation (Data data, vector<vector<double>> correlation) {
  frontier results;
  double return_rate = 0;
  for (int i = 0; i < 26; i++) {
    return_rate = return_rate + 0.01;
    int negative = 1;
    Data newdata;
    MatrixXd xlumbda = xlumbda_calculation (data, correlation, return_rate);
    for (int j = 0; j < xlumbda.rows() - 2; j++) {
      newdata.name.push_back(data.name[j]);
      newdata.avg_return.push_back(data.avg_return[j]);
      newdata.std_deviation.push_back(data.std_deviation[j]);
    }
    vector<vector<double>> newcorrelation;
    for (int j = 0; j < xlumbda.rows() - 2; j++) {
      vector<double> row;
      for (int k = 0; k < xlumbda.rows() - 2; k++) {
        row.push_back(correlation[j][k]);
      }
      newcorrelation.push_back(row);
    }  
    while (negative == 1) {
      negative = 0;
      for (int j = xlumbda.rows() - 3; j > -1; j--) {     
        if (xlumbda(j, 0) <= 0) {
          xlumbda(j, 0) = 0;
          negative = 1;
        }
        if (xlumbda(j, 0) == 0) {
          newdata.name.erase(newdata.name.begin() + j);
          newdata.avg_return.erase(newdata.avg_return.begin() + j);
          newdata.std_deviation.erase(newdata.std_deviation.begin() + j);
        }
      }
      if (negative == 0) {
        goto outloop;
      }
      for (int j = xlumbda.rows() - 3; j > -1; j--) {
        if (xlumbda(j, 0) == 0) {
          newcorrelation.erase(newcorrelation.begin() + j);
        }
      }
      for (int j = 0; j < (int)newcorrelation.size(); j++) {
        for (int k = xlumbda.rows() - 3; k > -1; k--) {
          if (xlumbda(k, 0) == 0) {
            newcorrelation[j].erase(newcorrelation[j].begin() + k);
          }
        }
      }
      xlumbda = xlumbda_calculation (newdata, newcorrelation, return_rate);
    }
    outloop:
    double vol = 0;
    for (int j = 0; j < (int)newdata.name.size(); j++) {
      for (int k = 0; k < (int)newdata.name.size(); k++) {
        vol = vol + xlumbda(j, 0) * xlumbda(k, 0) * newcorrelation[j][k] * newdata.std_deviation[j] * newdata.std_deviation[k] ;
      }
    } 
    results.ror.push_back(return_rate * 100);
    results.volatility.push_back(sqrt(vol) * 100);
  }
  return results;
}
  

frontier unrestricted_calculation (Data data, vector<vector<double>> correlation) {
  frontier results;
  int n = (int)data.name.size();
  MatrixXd covariance_matrix(n,n);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      covariance_matrix(i, j) = correlation[i][j] * data.std_deviation[i] * data.std_deviation[j];
    }
  }
  MatrixXd A(2,n);
  for (int i = 0; i < n; i++) {
    A(0, i) = 1;
    A(1, i) = data.avg_return[i]; 
  } 
  MatrixXd b(1,n+2);
  for (int i = 0; i < (n + 2); i++) {
    b(0, i) = 0;
  }
  b(0, n) = 1;
  b = b.transpose();   
  MatrixXd big(n+2,n+2);
  Matrix2d zero;
  zero << 0, 0, 0, 0; 
  MatrixXd xlumbda(1,n+2);
  xlumbda = xlumbda.transpose();
  big << covariance_matrix, A.transpose(), A, zero;
  double return_rate = 0;
  for (int i = 0; i < 26; i++) {
    return_rate = return_rate + 0.01;
    b((n + 1), 0) = return_rate;
    xlumbda = big.inverse() * b;
    double vol = 0;
    for (int j = 0; j < n; j++) {
      for (int k = 0; k < n; k++) {
        vol = vol + xlumbda(j, 0) * xlumbda(k, 0) * covariance_matrix(j, k) ;
      }
    }
    results.ror.push_back(return_rate * 100);
    results.volatility.push_back(sqrt(vol) * 100);
  }
  return results;
}
  
void print_output (frontier results) {
  cout << "ROR,volatility" << endl;
  for (int i = 0; i < 26; i++) {
    cout << setprecision(1) << setiosflags(ios::fixed) << results.ror[i] << "%" << "," << setprecision(2) << setiosflags(ios::fixed) << results.volatility[i]  << "%" << endl;  
  }
}    
          

int main(int argc, char ** argv) {
  if ((argc != 3) && (argc != 4)) {
    cerr << "Argument Number Error!" << endl;
    exit(EXIT_FAILURE);
  }
  Data data_set; //定义原始数据集
  string file1 = argv[1];
  string file2 = argv[2];  
  if (argc == 4) {
    file1 = argv[2];
    file2 = argv[3];
  }   
  ifstream inFile(file1);
  if (!inFile.good()) {
    cerr << "Input File Error!" << endl;
    exit(EXIT_FAILURE);
  }
  string lineStr; //装数据行
  while (getline(inFile, lineStr)) {
    stringstream ss1(lineStr); 
    string str1;
    double d;
    getline(ss1, str1, ',');
    //if (ss1.empty()) {
      //cerr << "Asset Name Error!" << endl;
      //exit(EXIT_FAILURE);
    //}
    data_set.name.push_back(str1);      
    getline(ss1, str1, ',');
    if (!isnum(str1)) {
      cerr << "Return Error!" << endl;
      exit(EXIT_FAILURE);
    }
    d = stod(str1);
    data_set.avg_return.push_back(d);
    getline(ss1, str1, ',');
    if (!isnum(str1)) {
      cerr << "Deviation Error!" << endl;
      exit(EXIT_FAILURE);
    }
    d = stod(str1);
    data_set.std_deviation.push_back(d); 
  }
  int n = (int)data_set.name.size(); 
  vector<vector<double>> correlation_matrix;
  correlation_matrix.resize(n);
  ifstream inFile1(file2);
  if (!inFile1.good()) {
    cerr << "Input File Error!" << endl;
    exit(EXIT_FAILURE);
  }  
  string lineStr1; //装数据行
  int k = 0;
  while (getline(inFile1, lineStr1)) {
    correlation_matrix[k].resize(n);
    stringstream ss1(lineStr1); 
    string str1;
    double d;
    int j = 0;
    while (getline(ss1, str1, ',')) {
      if (!isnum(str1)) {
        cerr << "Correlation Error!" << endl;
        exit(EXIT_FAILURE);
      }
      d = stod(str1);      
      correlation_matrix[k][j] = d;
      j++;
    }
    k++;
  }
  if ((int)correlation_matrix.size() != n) {
    cerr << "Correlation Error!" << endl;
    exit(EXIT_FAILURE);
  } 
  for (int i = 0; i < n; i++) {
    if ((int)correlation_matrix[i].size() != n) {
      cerr << "Correlation Error!" << endl;
      exit(EXIT_FAILURE);
    }
  }          
  frontier result;
  if (strcmp(argv[1], "-r") == 0) {
    result = restricted_calculation (data_set, correlation_matrix); 
  }
  else {
    result = unrestricted_calculation (data_set, correlation_matrix); //执行计算
  }
  print_output (result); //输出计算结果
  return EXIT_SUCCESS;
}